###4.2 微分の連鎖率
損失関数${\bf E}$の微分を求める前に、必要な数学について見ておきましょう。
####4.2.1 合成関数の微分

$x$から$u$が定まり、$u$から$f$が定まるとき、以下が成り立つ。
$$
\frac{df}{dx} = \frac{df}{du} \cdot \frac{du}{dx}
$$

合成関数の微分の例
$$
f(x) = 2(x^2 + x + 1)^3
$$
を微分します。ここで上記式を以下のように二つの関数に分けます。
$$
f = 2u^3 \\
u = x^2 + x + 1
$$
それぞれを微分します。
$$
f' = \frac{df}{du} = 6u^2 = 6(x^2+x + 1)^2\\
u' = \frac{du}{dx} = 2x + 1
$$
最後に微分の連鎖率の定義により、
$$
\frac{df}{dx} = \frac{df}{du} \cdot \frac{du}{dx} = 6(x^2+x + 1)^2(2x+1)
$$
となります。

####4.2.2 微分の連鎖率
連鎖率は合成関数の微分の多変数関数への拡張です。
$(x, y)$から$(u, v)$が定まり、$(u, v)$から$f$が定まるとき、以下が成り立つ。
$$
\frac{\partial f}{\partial x} = \frac{\partial f}{\partial u} \frac{\partial u}{\partial x} + \frac{\partial f}{\partial v} \frac{\partial v}{\partial x} \\
\frac{\partial f}{\partial y} = \frac{\partial f}{\partial u} \frac{\partial u}{\partial y} + \frac{\partial f}{\partial v} \frac{\partial v}{\partial y} \tag{4.2.1}
$$

微分の連鎖率の例
$$
f(x,y)=(x^2+y^2)\sin{xy}
$$
の$\frac{\partial f}{\partial x}$を求めます。
$$
u(x, y) = x^2 + y^2 \\
v(x, y) = \sin{xy}
$$
とおくと、
$$
f = uv \\
\frac{\partial f}{\partial x} = \frac{\partial f}{\partial u} \frac{\partial u}{\partial x} + \frac{\partial f}{\partial v} \frac{\partial v}{\partial x} \\
= v(2x) + u(y \ \cos{xy}) 
= 2x\ \sin{xy} + (x^2y+y^3)\cos{xy}
$$

ここで式(4.2.1)を行列の形で表すと、
$$
{\bf J}=\begin{pmatrix}
\frac{\partial f}{\partial x} & \frac{\partial f}{\partial y} \\
\end{pmatrix}  \
{\bf J}_A=\begin{pmatrix}
\frac{\partial u}{\partial x} & \frac{\partial u}{\partial y} \\
\frac{\partial v}{\partial x} & \frac{\partial v}{\partial y} \\
\end{pmatrix} \
{\bf J}_B = \begin{pmatrix}
\frac{\partial f}{\partial u} & \frac{\partial f}{\partial v}
\end{pmatrix} \\
{\bf J} = {\bf J}_B{\bf J}_A \tag{4.2.2}
$$
このように導関数を要素に持つ行列${\bf J}, {\bf J}_A, {\bf J}_B$をヤコビ行列と呼びます。
一般に$(x_1, \cdots, x_{\ell})$から$(u_1,\cdots, u_m)$が定まり、$(u_1,\cdots, u_m)$から$(f_1,\cdots, f_n)$が定まるとしたとき、それぞれのヤコビ行列を${\bf J}_A, \ {\bf J}_B$とします。このとき$(f_1, \cdots, f_n)$のヤコビ行列は${\bf J}_B{\bf J}_A$となります。
上記をさらに一般化して、$i$を中間変数も含めた変数の数とすると、関数$f$の微分は$i$個のヤコビ行列の積として表すことができます。
$$
{\bf J} = {\bf J}_i{\bf J}_{i-1}{\bf J}_{i-2} \cdots {\bf J}_1 \ \ \ (i > 1) \tag{4.2.3}
$$
