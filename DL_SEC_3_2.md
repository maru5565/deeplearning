###3.2 過学習への対応
深層学習に限らず統計的にデータからパラメーターを推定する学習モデルでは、しばしば訓練データ(学習に使うデータ)に過剰に適合したモデルが構築されることがあります。この状態のことを**過学習**もしくは過適合と言います。過学習のモデルはテスト時の未知のデータに対して弱く良いモデルとは言えません。逆に適度にデータに適合したモデルを**汎化**能力が高いモデルと言います。
過学習はモデルの自由度が高い場合に起きやすいのですが、やみくもに自由度を下げることは逆に多様な表現を損ねてしまうことが考えられます。そこでモデルに適度な制限を加える方法を考えます。古典的な方法として、モデルに正則化項を加えて表現できる範囲に制約を加える方法があります。

####3.2.1 正則化
損失関数$E$に正則化項$\lambda \frac{1}{p} \|{\bf w}\|_p^p$を加えます。
$$
E({\bf w})  + \lambda \frac{1}{p} \|{\bf w}\|_p^p = E({\bf w}) + \lambda \frac{1}{p} \sum_i|w_i|^p \tag{3.2.1}
$$
$p=1, 2$のときをそれぞれL1、L2正則化と言いそれぞれ特徴があります。L1正則化ではパラメーターのいくつかがゼロになるように学習が収束し、スパースな結果が得られます。L2正則ではL1よりは滑らかに収束します。L1、L2正則化によりなぜ過学習が防げるのかについての詳細は、機械学習の文献等あちこちで紹介されていますので割愛しますが、イメージとしてはパラメーターの動ける範囲に制限を設けることにより、入力データに過剰にフィットすることを避け適度に汎化したモデルを構築できるということになります。
ニューラルネットワークの学習においてはしばしばL2正則化が用いられるため、こちらで説明をしていきます。
$$
E({\bf w})  + \lambda \frac{1}{2} \|{\bf w}\|_2^2 = E({\bf w}) + \lambda \frac{1}{2} \sum_i|w_i|^2 \tag{3.2.2}
$$
よって、ミニバッチでの更新式は以下のようになります。
$$
{\bf w} \leftarrow {\bf w} - \eta\frac{1}{B}\Biggl(\frac{\partial E_{p}}{\partial {\bf w}} + {\lambda} |{\bf w}| \Biggr)\tag{3.2.3}
$$
ここで$\lambda$は正則化の強さを調整するハイパーパラメーターで、通常$0.01$など小さな値を設定します。大きすぎるとモデルの平滑化が進みすぎて雑なモデルになり、小さすぎると正則化の効果が薄れてしまうため、問題によって適切に調整を行います。

####3.2.2 ドロップアウト
**図.3.2.1**
![enter image description here](https://lh3.googleusercontent.com/-oPQE8PuyeRQ/V9ecWZXCtuI/AAAAAAAAAV0/REpwIbpmR08ozh8sfwePr_ht89oR17y1wCLcB/s0/%25E3%2582%25B9%25E3%2582%25AF%25E3%2583%25AA%25E3%2583%25BC%25E3%2583%25B3%25E3%2582%25B7%25E3%2583%25A7%25E3%2583%2583%25E3%2583%2588+2016-09-13+15.26.34.png "スクリーンショット 2016-09-13 15.26.34.png")

ドロップアウトとはニューラルネットワークにおいて過学習を防ぐための有効な手段です。図3.2.1のように学習時にネットワークを構成するユニットの一部をランダムに無いものとして学習します。一回のパラメーター更新が終わると、またランダムに無効化するユニットを選び直して学習を続けます。学習終了後のテスト時にはすべてのユニットを用います。
どのくらいのユニットを無効化するかを示すハイパーパラメーター$p$を$[0, 1]$の範囲で確率として定義し、入力層で$p=0.3$、隠れ層(中間層)で$p=0.5$あたりに調整するのが一般的です。なお、出力層は通常ドロップアウトしません。また入力層は入力データが少ない場合にはドロップアウトしないこともよくあります。
これによって過学習が防げる理由の１つは、ドロップアウトにより次元が削減され、学習時に本来あるべきユニットの近傍のユニットが役割を肩代わりするように学習が進むことです（より低次元で学習することにより汎化が進む）。２つ目は更新のたびに無効化するユニットが変更になることから、更新のたびに異なる学習器を訓練して平均をとっていることと同じです。一般に異なる学習器で学習したモデルの平均は汎化能力が高まります。これをアンサンブル学習と言います。
実際のプログラミングでは、学習時には順・逆伝搬両方で無効化対象ユニットの出力をゼロにすることによりユニットの無効化を実現します（しかし定義からすると厳密にはこの方法は間違っており、本来は無効化対象のユニットには入力すら無いはずですが、十分に同じ効果を得られます。また順・逆伝搬については次章以降で詳しく説明します）。また確率$p$でゼロが出力されるので、全体の出力はドロップアウトしない場合にくらべて確率$1-p$だけ減ってしまっています。本来のドロップアウト無しの確率$(=1)$に近づけるように、ゼロ以外の出力を$\frac{1}{1-p}$倍にして補正します。たとえば、$p=0.3$の確率でドロップアウトする場合、$0*0.3+ \frac{1}{1-0.3}*0.7 = 1$のようになります。
ひとつ注意点として、ドロップアウトを導入すると、ユニットを無効化した数に比例してドロップアウト無しの時と比べて学習の進み具合が遅くなります。そのため学習回数をより多くとる必要があります。
