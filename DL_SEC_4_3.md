###4.3 後退型自動微分
後退型自動微分とは、4.2で見てきた微分の連鎖率を用いて多変数関数の変数すべてについて微分を行う具体的な方法です。
####4.3.1 後退型自動微分の数式表現
最初に例として$f(x_1, x_2) = x_1x_2 + \sin{x_1}$の微分を考えてみます。
まずシードを$1$、つまり$\frac{\partial f}{\partial w_5} = 1$から始めて、各演算のタイミングで中間変数$w_i$を作りながら微分の連鎖率の定義にしたがって$w_i \rightarrow f$の導関数を掛けていきます。最後の変数$x_1, \ x_2$まで連鎖した時点で計算終了です。
$$
\frac{\partial f}{\partial w_5} = \frac{\partial w_5}{\partial w_5} = 1 \\
{\displaystyle \frac{\partial w_5}{\partial w_4} = \frac{\partial w_5}{\partial w_5}\cdot \frac{\partial w_5}{\partial w_4} = \frac{\partial w_5}{\partial w_5}\cdot\frac{\partial (w_3+w_4)}{\partial w_4}=\frac{\partial w_5}{\partial w_5}} \cdot 1\\
{\displaystyle \frac{\partial w_5}{\partial w_3} = \frac{\partial w_5}{\partial w_5}\cdot \frac{\partial w_5}{\partial w_3} = \frac{\partial w_5}{\partial w_5}\cdot\frac{\partial (w_3+w_4)}{\partial w_3}=\frac{\partial w_5}{\partial w_5}} \cdot 1 \\
{\displaystyle \frac{\partial w_5}{\partial w_2} = \frac{\partial w_5}{\partial w_3} \cdot \frac{\partial w_3}{\partial w_2} = \frac{\partial w_5}{\partial w_3} \cdot \frac{\partial (w_1 \cdot w_2)}{\partial w_2} = \frac{\partial w_5}{\partial w_3}\cdot w_1} \\
{\displaystyle \frac{\partial w_5}{\partial w_1} = \frac{\partial w_5}{\partial w_3} \cdot \frac{\partial w_3}{\partial w_1} + \frac{\partial w_5}{\partial w_4} \cdot \frac{\partial w_4}{\partial w_1} \\
= \frac{\partial w_5}{\partial w_3} \cdot \frac{\partial (w_1 \cdot w_2)}{\partial w_1}+ \frac{\partial w_5}{\partial w_4} \cdot \frac{\partial \sin{w_1}}{\partial w_1}  \\
= \frac{\partial w_5}{\partial w_3} \cdot w_2 + \frac{\partial w_5}{\partial w_4} \cdot \cos{w_1}} \\
(x_1 = w_1, \ x_2 = w_2) 
$$

####4.3.2 後退型自動微分の図表現
4.3.1の数式を図にマッピングすると以下のようになります。

**図4.3.1**
![enter image description here](https://lh3.googleusercontent.com/-rIrVKXraAYA/V9kUoVbvyBI/AAAAAAAAAYU/YgsME2wHBJcwYqcCbmDAQlj5RoU7H_BowCLcB/s0/%25E3%2582%25B9%25E3%2582%25AF%25E3%2583%25AA%25E3%2583%25BC%25E3%2583%25B3%25E3%2582%25B7%25E3%2583%25A7%25E3%2583%2583%25E3%2583%2588+2016-09-14+18.12.26.png "スクリーンショット 2016-09-14 18.12.26.png")

微分の連鎖率の定義や図4.1.1の計算グラフで、右から左に向かってヤコビ行列を掛けている処理が、図では上から下に向かっての処理に相当します。

####4.3.3 多層ニューラルネットワークにおける各パラメーターの微分
さて、ここまで見てきた微分の連鎖率を使っていよいよ多層ニューラルネットワークにおける各パラメーターにおける微分について見ていきます。
多層ニューラルネットワークにおいて、入力をミニバッチの行列$X$とした場合に第$l = (1, 2, \cdots, L)$階層は以下のようになっていました。ここで$f$は活性化関数、${\bf W}$は重み行列、${\bf B}$はバイアスベクトルです。
$$
{\bf X} = {\bf U}^{(1)}
$$
$$
{\bf Z}^{(l)} = f^{(l)}({\bf U}^{(l)}) \tag{4.3.1}
$$
$$
{\bf U}^{(l+1)} = {\bf W}^{(l)}{\bf Z}^{(l)} + {\bf B}^{(l)} \tag{4.3.2}
$$
また、分類問題における出力層の活性化関数でもあるソフトマックス・クロスエントロピー誤差関数${\bf E}_p$に関しては、以下のように表されました。（ひとつのパターンベクトル$p$を選んだ状態）
$$
 {\bf E}_p = -\sum_c^C {\bf T}_{c} \odot {\rm log}({\bf Z}_{c})  \tag{4.3.3}
$$
ただし、${\bf Z}_c$はソフトマックス関数の出力値です。
前節までに見てきたように、まずは出力層に近い層から順に入力層に向けて微分の連鎖率を使いながら導関数を求めていきます。
まず出力層の損失関数の微分から見てみます。式(2.2.15)を一つのパターンベクトルの場合について考えた時の損失関数$E_p$は、
$$
E_p = -\sum_c t_c \log{z_c}
$$
です（右辺の添字${}_p$は省略）。いま出力層$L$のユニット$j$に注目して、$E_p$の微分$\frac{\partial E_p}{\partial u_j^{(L)}}$を求めます。
ソフトマックス関数の微分は、
$$
\dfrac{\partial z_c}{\partial u_j}= \begin{cases}z_c(1-z_c)& c=j \\ -z_c z_j & c \neq j\end{cases}
$$
であり（証明は省略）、$\sum_ct_c = 1$であることを考慮すると、
$$
\frac{\partial E_p}{\partial u_j^{(L)}} = -\sum_c{t_c} \frac{1}{z_c} \frac{\partial z_c}{\partial { u_j}^{(L)}} \\
= -t_j(1-z_j) - \sum_{c \neq j}t_c(-z_j) \\
= \sum_c t_c(z_j - t_j) \\
= z_j - t_j \tag{4.3.4}
$$
となり、損失関数の微分はソフトマックス関数の出力と教師データとの差になります。後で説明する中間層の微分の説明と整合性をとるため、$\delta_j^{(L)} =  z_j^{(L)} - t_j $と定義しておきます。

ミニバッチの行列で表すと以下のようになります。
$$
\Delta^{(L)} = \frac{\partial {\bf E}_p}{\partial {\bf U}^{(L)}} = {\bf Z}^{(L)} - {\bf T} \tag{4.3.5}
$$
出力層でのパラーメーターは$u$だけですので、出力層での微分の計算は以上です。

次に活性化関数$f(u_j)^{(l)}$の微分を見ていきます。例えば、ロジスティック関数の微分は以下になります。
$$
f'(u_j^{(l)}) = f(u_j^{(l)})(1-f(u_j^{(l)})) \tag{4.3.6}
$$

次に中間層の微分$\frac{\partial E_p}{\partial w_{ji}^{(l)}}$を見ていきます。例のごとくバイアス$b_j$は重み$w_0$で表すとします。
$$
u_j^{(l)} = \sum_i w_{ji}^{(l)}z_i^{(l-1)} \tag{4.3.7}
$$
の関係でもわかる通り、$w_{ji}^{(l)}$は$u_j^{(l)}$の中にあることから、微分の連鎖率を使うと、
$$
\frac{\partial E_p}{\partial w_{ji}^{(l)}} = \frac{\partial E_p}{\partial u_j^{(l)}} \frac{\partial u_j^{(l)}}{\partial w_{ji}^{(l)}} \tag{4.3.8}
$$
と分解できます。ここでまた式(4.3.7)の関係を使うと、式(4.3.8)の右辺第２項はただちに以下になります。
$$
\frac{\partial u_j^{(l)}}{\partial w_{ji}^{(l)}} = z_j^{(l-1)} \tag{4.3.9}
$$
次に式(4.3.8)の右辺第１項を見ていきます。
$l$層のユニット$j$と、$l+1$層のユニット$k$の関係に注目すると、$j$の出力は$k$のすべてのユニットに影響を与えまます。したがって微分の連鎖率より以下のように分解できます。
$$
\frac{\partial E_p}{\partial u_j^{(l)}} = \sum_k \frac{\partial E_p}{\partial u_k^{(l+1)}} \frac{\partial u_k^{(l+1)}}{\partial u_j^{(l)}} \tag{4.3.10}
$$
ここで、
$$
\delta_j^{(l)} = \frac{\partial E_p}{\partial u_j^{(l)}}
$$
を定義すると、式(4.3.10)は、以下になります。
$$
\delta_j^{(l)} = \sum_k \delta_j^{(l+1)} \frac{\partial u_k^{(l+1)}}{\partial u_j^{(l)}}
$$
さらに、
$$
u_k^{(l+1)} = \sum_j w_{kj}^{(l+1)} z_j^{(l)} = \sum_j w_{kj}^{(l+1)} f(u_j^{(l)})
$$
であることから、
$$
\delta_j^{(l)} = \sum_k \delta_k^{(l+1)} (w_{kj}^{(l+1)} f'(u_j^{(l)})) \tag{4.3.11}
$$
となります。
したがって、式(4.3.8)は以下のように求まります。
$$
\frac{\partial E_p}{\partial w_{ji}^{(l)}} = \delta_j^{(l)} z_i^{(l-1)} \tag{4.3.12}
$$


式(4.3.11)および式(4.3.12)をミニバッチの行列の形で表すとそれぞれ以下のようになります。
$$
\Delta^{(l)} = f^{(l)'}(\bf U^{(l)}) \odot ({\bf W}^{(l+1){\mathrm T}} \Delta^{(l+1)}) \tag{4.3.13}
$$
$$
\frac{\partial {\bf E}_p}{\partial {\bf W}^{(l)}} = \Delta^{(l)} {\rm Z}^{(l-1){\mathrm T}} \tag{4.3.14} 
$$
ただし、実際にはバイアスを別に扱うので、バイアスベクトル${\bf b}$についての${\bf E}_p$の微分を以下に示します。
$$
\frac{\partial {\bf E}_p}{\partial {\bf b}} = \Delta^{(l)} {\bf v}^{\mathrm T} \tag{4.3.15}
$$
 ただし、${\bf v}$はすべての要素が$1$のベクトルです。

$l$層のニューラルネットワーク全体の損失関数の微分についてまとめると、以下のようになります。
まず、式(4.3.5)にて出力層の微分を求めます。式(4.3.13、4.3.14、4.3.15)を使って出力層($l=L$)から入力層($l=1$)に向けて、順計算とは逆向きの方向にそれぞれのパラメーターの微分を求めていき、入力層に達した段階で計算を終了します。計算が終了した時点ですべてのパラメーターの微分が完了していることになります。
これら一連の計算方法を**誤差逆伝搬法（ Back Propagation: BP）**といい、式中の$\delta$($\Delta$)を用いて出力層から入力層に向かって再起的に計算を繰り返すことによりすべてのパラメーターの微分を計算できます。これは一般的には前節の後退型自動微分と呼べますが、あえて違いに言及するとすれば$\delta$を明示的に計算・使用するのが誤差逆伝搬法、単に計算グラフにおける微分の連鎖率の適用と考えるのが後退型自動微分というイメージでよいのではないかと思います。やっていることは同じですが、実際のプログラミングの際には、後退型自動微分とみなして実装したほうが$\delta$をハードコードするよりも汎用性を保つには都合がよいと思われ、本書で紹介するプログラムもこちらの立場を取っています。ただし、プログラムの実行速度のことを考えるなら、$\delta$をハードコードするほうに分があるのは明らかです。どのように実装するかは結局のところ目的に応じて異なるものになるでしょう。


##5章 事前学習
###5.1 オートエンコーダー（De-noising）
重みより
###5.2 ユニットのスパース化（L1正則化, KL)


##6章 RNN
### RNN
### LSTM, GRU
### Seq2Seq


やっぱり１章にC++とCUDAの説明をもってくるべき？



